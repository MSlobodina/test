const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const scss = () => {
   return gulp.src('scss/*.scss')
     .pipe(sass().on('error', sass.logError))
     .pipe(gulp.dest('css/'))
}

gulp.task('scss', scss);

gulp.task('watch', () => {
  gulp.watch('scss/*.scss', scss)
})